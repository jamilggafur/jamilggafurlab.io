---
layout: page-fullwidth
title: "Public Works"
permalink: "/publication/"
header: no
---

## 2024
- [**Paper:**](https://web.cvent.com/event/f318e73c-2230-432a-a044-b75625020543/websitePage:afd80266-008e-414b-9f94-2fd9b4dd1924?session=d0cb8e47-b8da-430b-bf61-fabc876d6739&shareLink=true) Gafur, J., "Adversarial Robustness and Explainability of Machine Learning Models," PEARC24, Accepted (Full Paper).
- [**Presentation:**](https://pasc24.pasc-conference.org/presenter/?uid=193643) Gafur, J., Goddard, S., Tripp, C., "How Lobotomizing Neural Networks Can Improve Performance," PASC24, Summer 2024.
- [**Poster:**](https://gss.grad.uiowa.edu/jakobsen-conference/jakobsen-graduate-development-series-research-showcase-march-23-2024) Gafur, J., Goddard, S., Tripp, C., "The Impact of XAI Features and Magnitudes in Model Pruning," Jakobsen Conference, Spring 2024.

## 2023
- [**Presentation:**](https://www.nrel.gov/docs/fy23osti/87115.pdf) Tripp, C., Egan, H., Bensen, E., Perr-Sauer, J., Wimer, N., Nag, A., Zisman, S., Gafur, J., "Green Computing Opportunities & Strategy," NREL AI Research Group, Fall 2023.
- [**Talk:**](https://us-rse.org/rse-escience-2023/abstracts/) Gafur, J., Lai, W., "Energy-Efficient Neural Network Pruning for Environmentally-Friendly AI/ML", Escience, Fall 2023.
- [**Talk:**](https://us-rse.org/usrse23/program/sessions/) Gafur, J., "Integrating Adversarial Observation: Enhancing Robustness, Interpretability, and Trustworthiness in Machine Learning Systems", US-RSE, Fall 2023.

## 2022
- [**Talk:**](https://www.iscb.org/glbio2023-programme/scientific-programme) Gafur, J., Lai, W., "Adversarial attack identifies conserved features of enhancer chromatin architecture", GLBIO Conference, Spring 2023.
- **Review:** Research Software Engineering, Organizer, 2023.
- **Review:** INTERSECT, Teaching Assistant, 2023.
- **Review:** USRSE, Volunteer, Teaching and Education, 2023.
- **Review:** NeurIPS, Artifact Reviewer, 2021.

## 2021
- [**Talk:**](https://www.slideshare.net/slideshow/non-equilibrium-molecular-simulations-of-polymers-under-flow-saving-energy-through-process-optimization-b6ed/266751964) Gafur, J., Bu, L., Crowley, M., "Non-equilibrium Molecular Simulations of Polymers under Flow: Saving Energy through Process Optimization", EERE Advanced Manufacturing Office (AMO) Conference, Summer 2021.
- **Review:** Scipy, Reviewer, 2021.

## 2020
- [**Package:**](http://conference.scipy.org.s3-website-us-east-1.amazonaws.com/proceedings/scipy2020/students.html) Scipy 2020 Conference: Pyaesar.
- [**Talk:**](http://conference.scipy.org.s3-website-us-east-1.amazonaws.com/proceedings/scipy2020/students.html) Gafur, J., Neill-Asanza, D.H., Manore, C., Fairchild, G., "Pyaesar: A Multi-Node Multi-Processor API", SciPy Conference 2020.

## 2019
- [**Paper:**](https://www.osti.gov/servlets/purl/1569584) Beryllium Parameter Study Technical Paper.
- **Code:** Particle Swarm Optimization (PSO) and Visualization.
- **Presentation:** Beryllium Strength Model Parameter Study Using a Particle Swarm Optimizer.
- **Talk:** Gafur, J., Tourange, E., Hickmann, K., Prime, M., "Calibration of Flyer Plate Impact Experiments using Particle Swarm Optimization (PSO) Strategies", ASME, 01-18-2019.

## 2018
- **Code:** Pyaesar High Performance Computing Python Embarrassingly Parallel Processing.
- **Presentation:** Forecasting Dengue in Brazil with Time Series Modeling in Parallel.
- **Talk:** Gafur, J., Kempfert, K., "Forecasting Dengue in Brazil with Time Series Modeling in Parallel", LANL Student Symposium, 08-03-2018.
- **Review:** STEM-Trek ScienceSlam, Judge, 2021.

## 2017
- **Article:** 2017 Domestic Nuclear Detection Office Summer Internship Program.
- **Poster:** Nuclear Material Particle Analysis Developing a Plugin for the MAMA Software.
- **Talk:** Gafur, J., Kempfert, K., "Nuclear Material Analysis: Developing a Plugin for the MAMA software", LANL Student Symposium, 07-26-2017.
