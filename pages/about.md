---
layout: page-fullwidth
title: "About Me"
permalink: "/about/"
header: no
---

<img src="../assets/img/headshots-47.jpg" alt="Jamil Gafur" style="float: left; margin-right: 20px; width: 300px;" />

My name is **Jamil Gafur**, and I am a Research Software Engineer (RSE) currently pursuing a Ph.D. in Computer Science at the University of Iowa. My research revolves around developing and validating domain-specific machine learning techniques, with a strong focus on explainable artificial intelligence (XAI) and heuristic optimization. I am particularly interested in applying these methods to bioinformatics and genomics, bridging the gap between computational methods and biological insights.


## Research Interests

Our team examines the effects of various pruning techniques on neural network performance, emphasizing the preservation of accuracy and validation metrics. Our methodology is designed to optimize computational and memory efficiency while maintaining model robustness against unexpected inputs and ensuring transparent decision-making processes. The results from our experiments indicate that our approach significantly improves resource efficiency without compromising accuracy or reliability. This research presents a robust framework for developing AI systems that effectively balance performance with interpretability, while prioritizing resource efficiency. By methodically identifying and removing noisy and redundant neurons, we achieve enhanced compression rates within the network.

## Professional Experience

Over the years, I have had the privilege of working at prestigious institutions such as Los Alamos National Lab (LANL) and the National Renewable Energy Lab (NREL), where I contributed to a variety of cutting-edge projects. My experience includes:

- Developing a cell-type invariant enhancer prediction platform using epigenomic chromatin datasets.
- Applying machine learning techniques to renewable energy for modular network validation.
- Optimizing code for high-performance computing environments, including the optimization of Fortran90 code for polymer physics simulations.

## Community Involvement

I am actively involved in the US Research Software Engineer Association (US-RSE) and enjoy mentoring others, contributing to training resources, and fostering community engagement. Whether through talks, presentations, or contributing to open-source projects, I am committed to advancing the field of research software engineering.

## Education

- **Ph.D. in Computer Science, Specialization in Explainable Machine Learning**  
  University of Iowa, Iowa City, Iowa (Fall 2020 - Current)

- **BS in Computer Science, Minor in Business Administration**  
  CUNY: Lehman College, Bronx, NY (Fall 2016 - Spring 2018)

- **AS in Computer Science**  
  SUNY: Dutchess Community College, Poughkeepsie, NY (Fall 2014 - Spring 2016)

## Collaborators

My work has been greatly enriched by collaborating with esteemed researchers in the field:


- **[Steve Goddard - Advisor](https://homepage.divms.uiowa.edu/~sgoddard/)**  
  Steve is a Professor at the University of Iowa, known for his expertise in real-time systems and dependable computing. Our collaboration has focused on optimizing machine learning models, particularly in exploring the impact of XAI features and model pruning on neural network performance.
- **[Charles Tripp - PI](https://research-hub.nrel.gov/en/persons/charles-tripp)**
    Dr. Tripp is a Senior Researcher at the National Renewable Energy Lab (NREL), where he specializes in renewable energy and grid integration. Our collaboration has focused on applying machine learning techniques to renewable energy for modular network validation, with a particular emphasis on optimizing code for high-performance computing environments.
- **[William Kai Lai- PI](https://cals.cornell.edu/william-lai)**  
  William is an Assistant Professor at Cornell University, where he specializes in bioinformatics and computational biology. Together, we have worked on developing a cell-type invariant enhancer prediction platform using epigenomic chromatin datasets. His guidance has been instrumental in our joint research aimed at advancing genome-wide enhancer identification techniques.


## Let's Connect

Feel free to check out my [CV](https://gitlab.com/JamilGafur_Work/professional_works/personal/Resume) or reach out via [email](mailto:Jamil-gafur@uiowa.edu). I am always open to discussing new ideas, collaborations, or simply chatting about science.

Yours sincerely,  
**Jamil Gafur**
