---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header: no

carousels:
  - images:
      - image: /assets/img/water_madison.jpg
      - image: /assets/img/water_switzerland.jpg
      - image: /assets/img/switzerland_city.jpg

widget1:
  title: "Research"
  text: 'My research focuses on resource efficient neural networks, explainable AI, and adversarial machine learning. I am passionate about developing innovative solutions to address the challenges in these areas.'
widget2:
  title: "Interests"
  text: 'My main interests is at the intersection of Research Software Engineering (RSE), Machine Learning, and Human Good! I want my work to be able to contribute to the society and make a positive impact on people’s lives.'
widget3:
  title: "Collaborations"
  text: 'I am open to collaborations and discussions on research topics. Feel free to reach out to me for potential projects, academic inquiries, or professional networking. Your ideas and contributions are welcome!'
#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#
callforaction:
  url: /about
  text: Learn more about me
  style: alert
permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---
